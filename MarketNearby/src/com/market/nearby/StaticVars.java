package com.market.nearby;


public class StaticVars {

	public static final String MARKET = "market";
	public static final String LAT = "latitude";
	public static final String LONG = "longitude";
	public static final String LOG_TAG = "tag";
	public static final String ACTION_CODE = "do";
	public static final String LAT_KEY = "lat";
	public static final String LONG_KEY = "lng";
	public static final String DB_NAME = "marketNearby";
	public static final int DB_VER = 280;

}
