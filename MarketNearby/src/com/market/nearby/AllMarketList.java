package com.market.nearby;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.market.nearby.R;

public class AllMarketList extends Activity {

	ListView listview;
	MarketListitemAdapter adapter;
	private DBAdapter db;
	private Cursor cursor;
	private ArrayList<MarketListItem> marketArray = new ArrayList<MarketListItem>();
	MarketListItem marketItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.market_list);
		DBConnect();

		int actionCode = getIntent().getIntExtra(StaticVars.ACTION_CODE, 0);
		if (actionCode == 0) {
			try {
				cursor = db.getAllMarkets();
			} catch (CursorIndexOutOfBoundsException e) {
				new AppsLogs(e);
			} catch (SQLiteException e) {
				new AppsLogs(e);
			} catch (NullPointerException e) {
				new AppsLogs(e);
			} catch (Exception e) {
				new AppsLogs(e);
			}
		} else {
			try {
				DateFormatSymbols dfs = new DateFormatSymbols(
						Locale.getDefault());
				String weekdays[] = dfs.getWeekdays();
				Calendar cal = Calendar.getInstance();
				int day = cal.get(Calendar.DAY_OF_WEEK);
				String nameOfDay = weekdays[day];

				cursor = db.getMarketsByDay(nameOfDay);
			} catch (CursorIndexOutOfBoundsException e) {
				new AppsLogs(e);
			} catch (SQLiteException e) {
				new AppsLogs(e);
			} catch (NullPointerException e) {
				new AppsLogs(e);
			} catch (Exception e) {
				new AppsLogs(e);
			}
		}
		listview = (ListView) findViewById(R.id.marketListview);
		MarketListItem.setCursor(cursor);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			String marketName = cursor.getString(cursor
					.getColumnIndex(DBAdapter.NAME));
			String marketLocation = cursor.getString(cursor
					.getColumnIndex(DBAdapter.LOCATION));
			String closeDay = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CLOSEDAY));
			if (actionCode==0) {
				marketArray.add(new MarketListItem(marketName, marketLocation,
						"Closed Day: " + closeDay));
			}else{
				marketArray.add(new MarketListItem(marketName, marketLocation,
						""));
			}
		}

		adapter = new MarketListitemAdapter(AllMarketList.this,
				R.layout.market_list_item, marketArray);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				cursor.moveToPosition(position);
				String marketName = cursor.getString(cursor
						.getColumnIndex(DBAdapter.NAME));
				Toast.makeText(AllMarketList.this, marketName,
						Toast.LENGTH_SHORT).show();
			}
		});

	}

	private void DBConnect() {
		db = new DBAdapter(AllMarketList.this);
		db.open();
	}
}
