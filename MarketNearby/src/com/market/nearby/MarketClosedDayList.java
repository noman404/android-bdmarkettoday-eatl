package com.market.nearby;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.market.nearby.R;

public class MarketClosedDayList extends Activity {

	Spinner days;
	ListView marketClosedDayList;
	private DBAdapter db;
	Cursor cursor;
	List<String> daySpinnerList = new ArrayList<String>();
	String day = "";
	private ArrayList<MarketListItem> marketArray = new ArrayList<MarketListItem>();
	MarketListItem marketItem;
	MarketListitemAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.close_day_market_list);
		DBConnect();
		
		days = (Spinner) findViewById(R.id.daySpinner);
		marketClosedDayList = (ListView) findViewById(R.id.closedayMarketList);
		TextView empty = (TextView) findViewById(R.id.empty);
		marketClosedDayList.setEmptyView(empty);

		setUpSpinner();
		ArrayAdapter<String> spinnerdataAdapter = new ArrayAdapter<String>(
				this, R.layout.spinner_txt_item, daySpinnerList);
		days.setAdapter(spinnerdataAdapter);
		days.setOnItemSelectedListener(new SpinnerOnItemSelectedListener());
		
	}

	public void setUpSpinner() {
		daySpinnerList.add("Sunday");
		daySpinnerList.add("Monday");
		daySpinnerList.add("Tuesday");
		daySpinnerList.add("Wednesday");
		daySpinnerList.add("Thursday");
		daySpinnerList.add("Friday");
	}

	public void setUpList(String d) {
		try {
			cursor = db.getAllMarketsByDay(d);
		} catch (CursorIndexOutOfBoundsException e) {
			new AppsLogs(e);
		} catch (SQLiteException e) {
			new AppsLogs(e);
		} catch (NullPointerException e) {
			new AppsLogs(e);
		} catch (Exception e) {
			new AppsLogs(e);
		}
		MarketListItem.setCursor(cursor);
		
		if (!marketArray.isEmpty()) {
			marketArray.clear();
		}
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			String marketName = cursor.getString(cursor
					.getColumnIndex(DBAdapter.NAME));
			String marketLocation = cursor.getString(cursor
					.getColumnIndex(DBAdapter.LOCATION));
			String closeDay = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CLOSEDAY));
			marketArray.add(new MarketListItem(marketName, marketLocation,
					"Closed Day: "+closeDay));
		}

		adapter = new MarketListitemAdapter(MarketClosedDayList.this,
				R.layout.market_list_item, marketArray);
		marketClosedDayList.setAdapter(adapter);
		marketClosedDayList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				cursor.moveToPosition(position);
				String marketName = cursor.getString(cursor
						.getColumnIndex(DBAdapter.NAME));
				Toast.makeText(MarketClosedDayList.this, marketName,
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void DBConnect() {
		db = new DBAdapter(MarketClosedDayList.this);
		db.open();

	}

	public class SpinnerOnItemSelectedListener implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			day = parent.getItemAtPosition(pos).toString();
			String temp = day;
			setUpList(temp);

			Toast.makeText(MarketClosedDayList.this, "please wait", Toast.LENGTH_SHORT)
					.show();

		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {

		}

	}
}
