package com.market.nearby;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

	public DBHelper(Context context) {
		super(context, StaticVars.DB_NAME, null, StaticVars.DB_VER);

	}

	// database table-column declaration
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE markets("
				+ "_id INTEGER PRIMARY KEY AUTOINCREMENT," + " name TEXT,"
				+ " location TEXT," + " items TEXT," + " latitude TEXT,"
				+ " longitude TEXT," + " closeday TEXT)");

		PRE_DEFINED_VALUES(db);
	}

	// pre defined values
	private void PRE_DEFINED_VALUES(SQLiteDatabase db) {

		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Jamuna Future Park','Kuril High way, Basundhara Gate, Dhaka','Clothing, Food, Mobile, TV, Refrigerator','23.813562','90.42432','Wednesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Aarong','1/1, Block A, Lalmatia, Mirpur Road, Dhaka','Clothing','23.7562519','90.3678958','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Alpona Plaza','New Elephant Road, Dhaka','Clothing, Cosmetic, Toys','23.738654','90.384672','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Baitul Mokkaram Market','Baitul Mokkaram, Dhaka, Closed on Friday','Islamic Books,Clothing','23.729709','90.4135835','Friday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Chadni Chawk Market','Elephant Road, Dhaka','Ladis Clothing, Cosmetics, Food centre','23.7344584','90.3844452','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Polwel Super Market','Naya Paltan, Dhaka','Clothing, Toys, Food centre','23.7361397','90.4153386','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Bangabandhu Stadium Market','Bangabandhu National Stadium, Dhaka','CD DVD, electronics','23.728148','90.413402','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Eastern Plaza','Hatirpool, Sonargaon Road, Dhaka','Playing Item, Clothing','23.727783','90.4143393','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Gausia','Mirpur Road, Dhaka','Clothing, Bag, Cosmetic, Shoes','23.734585','90.384931','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Govt. New Market','Azimpur, Dhaka','Clothing, Watch, Bag, Cosmetic Books','23.7330136','90.3849095','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Navana Shoopping Center','Gulshan-1, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic,  Food centre','23.91486','90.4641023','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Karnophuli Garden City','Shantinagar, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic,  Food centre','23.739909','90.4103104','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Mouchak Market','Malibagh, Dhaka','Clothing, Bag, Cosmetic, Shoes','23.7456317','90.4127656','Thrusday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Riffles/Shimanta Square','BDR Gate, Zigatola, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic, Camera,  Mobile, Watch,  Food centre','23.7378047','90.3771726','Tesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Rapa Plaza','Mirpur Road, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.7559618','90.3757315','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Ramna Bhadan','Bangabandhu Avenue, Dhaka','Clothing, Mobile, Cosmetic','23.7264746','90.4113898','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Rajluxmi Complex','Uttara Model Town, Uttara, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic, Mobile, Watch,  Food centre','23.8641467','90.4000844','Wednesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('A.R Plaza','Dhanmondi, Dhaka','Clothing, Mobile, Computer','23.7548988','90.3753258','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('UAE Moitri Complex','Kemal Ataturk Avenue, Banani, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.794144','90.402339','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Vishal Centre','Moghbazar, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7508805','90.4078715','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Banani Supermarket','Banani, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic, Mobile, Watch,  Food centre','23.7933321','90.4064853','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('DCC Market','Gulshan, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic, Mobile, Watch,  Food centre','23.779669','90.414966','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Gulshan Shopping Center','Gulshan, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7811478','90.4179387','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Holland Centre','Gulshan, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.7825272','90.4247622','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Moghbazar Municipal Market','Mohakhali, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7510402','90.4053864','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Shahin Super Market','Dhaka Cantonment, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.8255884','90.3956955','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Mutalib Plaza','Hatirpool, Dhaka','Clothing, Toys, Mobile','23.742028','90.3925527','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Eastern Mallika','Old Elephant Road, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7370397','90.3863727','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Orchard Point','Mirpur Road, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7444716','90.3826184','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Royal Plaza','Mirpur, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7420243','90.3830278','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Capital Market','Mirpur Road, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.8141672','90.3664769','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('BCS Computer City','Aagargaon, Dhaka','Computer, Mobile, Camera','23.7787655','90.3799957','Sunday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Karnafuly City Garden','Shantinagar, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic','23.739882','90.411099','Monday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Bashundhara City','Panthapath, Karwan Bazar','Clothing, Toys, Playing Item, Shoes, Cosmetic, Mobile, Watch, Golden Item, Food centre','23.7511013','90.3915498','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Multiplan Center','New Elephant Road, Dhaka','Computer, Mobile, Camera','23.7379671','90.3857701','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Bishal Centre','Moghbazar, Dhaka','Clothing, Toys, Mobile, Cosmetic','23.749277','90.4077033','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Palwel  Super Market','Noya Paltan, Dhaka','Clothing, Toys, Playing Item, Shoes, Cosmetic','23.7362328','90.4155923','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Farmview Super Market','Farmget, Dhaka','Clothing, Toys, DVD','23.7577118','90.3904502','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Mascot Plaza','Uttara, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.874384','90.4008178','Wednesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Fortune Shopping Mall','Mouchak, Dhaka','Clothing, Toys, Mobile, Cosmetic, Food centre','23.7462034','90.4130438','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Gulistan Sports Market','Gulistan, dhaka','Clothing, Mobile, Sports, Electronics, Whole sales','23.72542','90.411457','Thursday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Nilkhet Book Market','New Market,Azimpur, Dhaka','Books, Stationary','23.733239','90.385872','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Hawkers Market','Mirpur Road, Dhaka College','T-shirt, Pants, Clothing','23.735245','90.3844193','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Bikrompur Super Market','Patuatuli, Islampur, Old- Dhaka','Clothing, electronics','23.7101032','90.4070193','Friday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Bangla Bazar Book Market','Bangla Bazar, Dhaka','Books, Papers, Whole Sell','23.7084922','90.4076738','Friday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Motalib Plaza','Mirpur Road, Dhaka','Phones, Electronics, Servicing','23.7420385','90.3923435','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Katabon Animal Market','Katabon Elephant Road, Dhaka','Birds, Cat, Dog, Animals, Aquarium, Home Decoration Service','23.738397','90.391007','Tuesday')");
		db.execSQL("INSERT INTO markets(name, location, items, latitude, longitude, closeday) "
				+ "VALUES ('Aziz Co-Operative Super Market','Shahbag, Elephant Road, Dhaka','Clothing, T-Shirt, Panjabi, Fatua, Shari','23.73861','90.392726','Thursday')");

	}

	// update table - dropping and renewing
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS markets");
		onCreate(db);

	}

}
