package com.market.nearby;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.market.nearby.R;

public class MainActivity extends ActionBarActivity {

	public ProgressDialog pd;
	public DBAdapter db;

	// String chkGPS;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		if (isNetAvailable(MainActivity.this) == false
				|| isGpsEnabled(MainActivity.this) == false) {
			final Dialog d = new Dialog(MainActivity.this);
			d.setContentView(R.layout.demo_dialog);
			d.setTitle("Attention Please!!!");
			d.setCancelable(false);
			Button demo = (Button) d.findViewById(R.id.okDemo);
			demo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					d.dismiss();
					finish();
				}
			});
			d.show();
		}

		new AsyncLoadDB().execute();
	}

	public void click(View v) {

		switch (v.getId()) {
		case R.id.allMarketsListBtn:

			Intent marketlistIntent1 = new Intent(MainActivity.this,
					AllMarketList.class);
			marketlistIntent1.putExtra(StaticVars.ACTION_CODE, 0);
			startActivity(marketlistIntent1);

			break;

		case R.id.todayMarketBtn:

			Intent marketlistIntent2 = new Intent(MainActivity.this,
					AllMarketList.class);
			marketlistIntent2.putExtra(StaticVars.ACTION_CODE, 1);
			startActivity(marketlistIntent2);

			break;

		case R.id.daywiseBtn:

			Intent closeDayIntent = new Intent(MainActivity.this,
					MarketClosedDayList.class);
			startActivity(closeDayIntent);

			break;
		}
	}

	public class AsyncLoadDB extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			pd = ProgressDialog
					.show(MainActivity.this, "Progressing Your System",
							"Processing...Please Wait", true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			db = new DBAdapter(MainActivity.this);
			db.open();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();

		}

	}

	private boolean isNetAvailable(Context context) {
		ConnectivityManager conMan = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		// mobile
		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState();

		// wifi
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.getState();

		if (mobile == NetworkInfo.State.CONNECTED
				|| mobile == NetworkInfo.State.CONNECTING
				|| wifi == NetworkInfo.State.CONNECTED
				|| wifi == NetworkInfo.State.CONNECTING) {
			return true;
		} else {
			return false;
		}

	}

	public boolean isGpsEnabled(Context context) {

		LocationManager locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void onPause() {
		if (pd.isShowing() || pd == null) {
			pd.dismiss();
		}
		super.onPause();
	}

}
