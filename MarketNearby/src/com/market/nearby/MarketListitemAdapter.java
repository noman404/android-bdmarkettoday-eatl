package com.market.nearby;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.market.nearby.R;

public class MarketListitemAdapter extends ArrayAdapter<MarketListItem> {

	Context context;
	int layoutResourceId;
	ArrayList<MarketListItem> marketLists = new ArrayList<MarketListItem>();
	Cursor cursor;
	private DBAdapter dbAdapter;
	MarketListItem marketItem;

	public MarketListitemAdapter(Context context, int layoutResourceId,
			ArrayList<MarketListItem> marketLists) {
		super(context, layoutResourceId, marketLists);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.marketLists = marketLists;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		ViewWrapper viewWrapper = null;

		if (item == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			item = inflater.inflate(layoutResourceId, parent, false);
			viewWrapper = new ViewWrapper();
			viewWrapper.marketName = (TextView) item
					.findViewById(R.id.marketName);
			viewWrapper.marketLocation = (TextView) item
					.findViewById(R.id.market_location);
			viewWrapper.closedDay = (TextView) item
					.findViewById(R.id.closedDay);
			viewWrapper.shoppingItems = (Button) item
					.findViewById(R.id.shoppingItem);
			viewWrapper.map = (Button) item
					.findViewById(R.id.marketLocationBtn);
			item.setTag(viewWrapper);
		} else {
			viewWrapper = (ViewWrapper) item.getTag();
		}

		MarketListItem marketItems = marketLists.get(position);
		viewWrapper.marketName.setText(marketItems.getMarketName());
		viewWrapper.marketLocation.setText(marketItems.getMarketLocation());
		viewWrapper.closedDay.setText(marketItems.getClosedDay());

		final int pos = position;
		dbCreate();

		viewWrapper.shoppingItems.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String item = cursor.getString(cursor
						.getColumnIndex(DBAdapter.ITEMS));
				Toast.makeText(context, item, Toast.LENGTH_SHORT).show();
			}
		});

		viewWrapper.map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String latitude = cursor.getString(cursor
						.getColumnIndex(DBAdapter.LATITUDE));
				String longitude = cursor.getString(cursor
						.getColumnIndex(DBAdapter.LONGITUDE));
				String name = cursor.getString(cursor
						.getColumnIndex(DBAdapter.NAME));
				Intent i = new Intent(context.getApplicationContext(),
						MapActivity.class);
				i.putExtra(StaticVars.MARKET, name);
				i.putExtra(StaticVars.LAT, Double.parseDouble(latitude));
				i.putExtra(StaticVars.LONG, Double.parseDouble(longitude));
				context.startActivity(i);
			}
		});

		return item;

	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(context);
		dbAdapter.open();
		cursor = MarketListItem.getCursor();

	}

	static class ViewWrapper {
		TextView marketName;
		TextView marketLocation;
		TextView closedDay;
		Button map;
		Button shoppingItems;

	}

}
