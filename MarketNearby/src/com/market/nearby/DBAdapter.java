package com.market.nearby;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DBAdapter {

	private Context context;
	private DBHelper dbHelper;
	public SQLiteDatabase db;

	public static final String ID = "_id", NAME = "name",
			LOCATION = "location", ITEMS = "items", LATITUDE = "latitude",
			LONGITUDE = "longitude", CLOSEDAY = "closeday",
			TABLE_MARKET = "markets";

	// set Context to access the database
	public DBAdapter(Context context) {
		this.context = context;
	}

	// open the database
	public DBAdapter open() throws SQLException {
		dbHelper = new DBHelper(context);
		db = dbHelper.getWritableDatabase();
		return this;
	}

	// close the database
	public void close() {
		dbHelper.close();
	}

	public Cursor getAllMarkets() throws NullPointerException,
			CursorIndexOutOfBoundsException, SQLiteException, Exception {

		Cursor cursor = db.query(DBAdapter.TABLE_MARKET, new String[] {
				DBAdapter.ID, DBAdapter.NAME, DBAdapter.LOCATION,
				DBAdapter.ITEMS, DBAdapter.LATITUDE, DBAdapter.LONGITUDE,
				DBAdapter.CLOSEDAY }, null, null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

	public Cursor getMarketsByDay(String day) throws NullPointerException,
			CursorIndexOutOfBoundsException, SQLiteException, Exception {

		Cursor cursor = db.rawQuery("SELECT * FROM " + DBAdapter.TABLE_MARKET
				+ " WHERE " + DBAdapter.CLOSEDAY + " NOT LIKE '%" + day + "%'",
				null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

	public Cursor getAllMarketsByDay(String day) throws NullPointerException,
			CursorIndexOutOfBoundsException, SQLiteException, Exception {

		String where = DBAdapter.CLOSEDAY + "='" + day + "'";
		Cursor cursor = db.query(DBAdapter.TABLE_MARKET, new String[] {
				DBAdapter.ID, DBAdapter.NAME, DBAdapter.LOCATION,
				DBAdapter.ITEMS, DBAdapter.LATITUDE, DBAdapter.LONGITUDE,
				DBAdapter.CLOSEDAY }, where, null, null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

}
